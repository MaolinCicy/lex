#include <stdio.h>


#ifndef UNUSED
#define UNUSED(x)       (void)x
#endif

int main(int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    printf("Hello World!");
}
