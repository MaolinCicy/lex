#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "dfa.h"

static void dfa_add_event(
    dfa_state_t *dfa_state_tmp, 
    const uint8_t event_char, 
    uint16_t next_state)
{
    dfa_state_tmp->event_arr[event_char] = next_state;
}

/* 创建确定自动机状态列表 */
/* 接收 (a|b)*abb 的DFA */

/**
状态0：起始状态
    接收 a 切换 2
    接收 b 切换 1

状态1：
    接收 a 切换 2
    接收 b 切换 3

状态2：
    接收 a 切换 2
    接收 b 切换 4

状态3：终止状态
    接收 a 切换 2
    接收 b 切换 1
    接收其它 跳出
**/
int init_dfa_state_list(dfa_state_t **dfa_state, uint16_t *state_num)
{
    const uint16_t state_number = 5;
    dfa_state_t *dfa_state_tmp = NULL;

    dfa_state_tmp = (dfa_state_t *)malloc(sizeof(dfa_state_t) * state_number);
    memset(dfa_state_tmp, 0x00, sizeof(dfa_state_t) * state_number);

    /* 
        状态1：失败状态
    */
    dfa_state_tmp[0].state_type = dfa_state_failed;

    /* 
        状态1：起始状态
            接收 a 切换 2
            接收 b 切换 1 
    */
    dfa_state_tmp[1].state_type = dfa_state_start;
    dfa_add_event(&dfa_state_tmp[1], 'a', 2);
    add_event(&dfa_state_tmp[1], 'b', 1);

    /* 
        状态2：
            接收 a 切换 2
            接收 b 切换 3 
    */
    dfa_state_tmp[2].state_type = dfa_state_intermediate;
    dfa_add_event(&dfa_state_tmp[2], 'a', 2);
    dfa_add_event(&dfa_state_tmp[2], 'b', 3);

    /* 
        状态3：
            接收 a 切换 2
            接收 b 切换 4
    */
    dfa_state_tmp[3].state_type = dfa_state_intermediate;
    dfa_add_event(&dfa_state_tmp[3], 'a', 2);
    dfa_add_event(&dfa_state_tmp[3], 'b', 4);

    /* 
        状态4：终止状态
            接收 a 切换 2
            接收 b 切换 1
            接收其它 跳出
    */
    dfa_state_tmp[4].state_type = dfa_state_final;
    dfa_add_event(&dfa_state_tmp[4], 'a', 2);
    dfa_add_event(&dfa_state_tmp[4], 'b', 1);

    *dfa_state = dfa_state_tmp;
    *state_num = state_number;
    return 0;
}

/* 释放DFA空间 */
int destroy_dfa_state_list(dfa_state_t *dfa_state)
{
    if(dfa_state != NULL)
    {
        free(dfa_state);
    }
    return 0;
}

/* 执行DFA */
/* return :
    -1 error
    0 continue
    1 finish */
int exec_dfa(
    dfa_state_t *dfa_state, 
    uint16_t state_num, 
    uint16_t *cur_state, 
    const uint8_t event_char)
{
    /* 状态总数大于当前状态 */
    assert(state_num > *cur_state);

    static uint16_t last_state = 0;
    last_state = *cur_state;

    printf("event_char:%c, cur_state:%d, ", event_char, *cur_state);
    *cur_state = dfa_state[*cur_state].event_arr[event_char];
    printf("next_state:%d \r\n", *cur_state);

    /* 执行到这里是没找 */
    if(dfa_state[*cur_state].state_type == dfa_state_failed)
    {
        if(dfa_state[last_state].state_type == dfa_state_final)
        {
            printf("dfa_state_final get an unexpected char!finished!\r\n");
            return 1;
        }
        else
        {
            printf("get an unexpected char!failed\r\n");
            return -1;
        }
    }
    return 0;
}