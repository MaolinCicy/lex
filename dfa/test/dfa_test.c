#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "dfa.h"

#ifndef UNUSED
#define UNUSED(x)       (void)x
#endif

int main(int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    uint16_t cur_char = 0;
    uint8_t teststr[] = "abaaabababb";

    dfa_state_t *dfa_state;
    uint16_t state_num;
    if(init_dfa_state_list(&dfa_state, &state_num))
    {
        return -1;
    }
    
    /* 设置起始状态 */
    uint16_t cur_state = 1;

    int ret = 0;

    while(ret == 0)
    {
        ret = exec_dfa(dfa_state, state_num, &cur_state, teststr[cur_char]);

        if(cur_char <= sizeof(teststr)/sizeof(uint8_t))
        {
            cur_char++;
        }
        else
        {
            ret = -1;
            break;
        }
    }

    if(destroy_dfa_state_list(dfa_state))
    {
        return -1;
    }

    if(ret == 1)
    {
        printf("lex success!");
    }
    else
    {
        printf("lex failed!");
    }
    return 0;
}

