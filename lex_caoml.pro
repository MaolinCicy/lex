######################################################################
# Automatically generated by qmake (3.1) Mon Mar 29 17:09:31 2021
######################################################################

TEMPLATE = app
TARGET = lex_caoml
INCLUDEPATH += include/
OBJECTS_DIR = $$PWD/output
DESTDIR = $$PWD
CONFIG += debug

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += include/mempool_api.h \
           dfa/include/dfa.h \
           nfa/include/nfa.h \
           nfa/test/nfa_test.h \
           utils/mempool/include/mempool.h
SOURCES += main.c \
