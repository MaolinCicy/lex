#ifndef __MEMPOOL_H__
#define __MEMPOOL_H__

#include <stdint.h>


typedef struct mempool_node_s
{
    struct mempool_node_s *next;
    void *data_addr;
}mempool_node_t;
typedef struct mempool_s
{
    /* 每一个块的大小 */
    uint32_t block_size;

    uint32_t pool_size;

    /* 内存块起始地址 */
    void *base_addr;

    mempool_node_t *free_node_list;
}mempool_t;

mempool_t *mempool_create(uint32_t block_size, uint32_t pool_size);
void *mempool_get_node(mempool_t * mp);
int mempool_put_node(mempool_t * mp, void * mp_node);
int *mempool_destory(mempool_t * mp);

#endif