#include <stdio.h>
#include "mempool.h"

#ifndef UNUSED
#define UNUSED(x)       (void)x
#endif

int main(int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    printf("mempool test!\r\n");
    mempool_t *mp = mempool_create(24, 2570);
    printf("%p ", mp);

    if(mp == NULL)
    {
        printf("mempool_create failed!\r\n");
    }
    else 
    {
        printf("mempool_create success!\r\n");
    }

    void *mp_node = NULL;
    for(uint32_t index=0;index < mp->pool_size;index++)
    {
        mp_node = mempool_get_node(mp);
        printf("index:%u, data_addr:%p\r\n", index, mp_node);
    }

    mempool_destory(mp);

    return 0;
}

