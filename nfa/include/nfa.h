#ifndef __NFA_H__
#define __NFA_H__

#include "stdint.h"
#include <sys/queue.h>

#define EMPTY_STR (256)

#define EVENT_PAGE_SIZE (64)
#define STATE_PAGE_SIZE (64)
#define ALIGN(size, align) ((size + align - 1) & (~(align - 1)))
#define ALIGN_EVENT_PAGE(size) ALIGN(size,EVNET_PAGE_SIZE)
/* 用状态转换表表示一个不确定有穷自动机 */
/* 与确定有穷自动机的差别在于，接收输入后的下一跳，
   确定有穷自动机是一个确定的状态，不确定的有穷自动机可能有几个状态 */
typedef enum
{
    nfa_state_failed = 0,
    nfa_state_start,
    nfa_state_intermediate,
    nfa_state_final,
}nfa_state_type_e;

typedef struct nfa_state_value_s
{
    uint32_t state_value;
    struct nfa_state_value_s * next;
}nfa_state_value_t;

typedef struct nfa_event_s
{
    uint32_t event;
    nfa_state_value_t *jump_state;

    struct nfa_event_s *next;
}nfa_event_t;

typedef struct nfa_state_s
{
    uint32_t state;
    nfa_state_type_e state_type;

    /* 字符串加空串 */
    nfa_event_t *event_list;
}nfa_state_t;

typedef struct nfa_ctx_s
{

    /* 状态数组首地址 */
    nfa_state_t *nfa_state;

    /* 状态数 */
    uint32_t nfa_state_num;

    /* 事件内存池 */
    mempool_t *nfa_event_mp;

    /* 一个事件可能跳转到多个状态 */
    mempool_t *nfa_state_value_mp;
}nfa_ctx_t;

void nfa_init(nfa_ctx_t *ctx);

#endif