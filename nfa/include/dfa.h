#ifndef __DFA_H__
#define __DFA_H__

#include "stdint.h"
#include <sys/queue.h>

/* 字符数 */
#define CHAR_NUB (256)

typedef enum
{
    dfa_state_failed = 0,
    dfa_state_start,
    dfa_state_intermediate,
    dfa_state_final,
}dfa_state_type_e;

typedef struct dfa_state_s
{
    uint16_t state;
    dfa_state_type_e state_type;

    /* 为了性能，直接使用256的数组 */
    uint16_t event_arr[CHAR_NUB];
}dfa_state_t;

int init_dfa_state_list(dfa_state_t **dfa_state, uint16_t *state_num);

int destroy_dfa_state_list(dfa_state_t *dfa_state);

int exec_dfa(
    dfa_state_t *dfa_state, 
    uint16_t state_num, 
    uint16_t *cur_state, 
    const uint8_t event_char);

#endif
