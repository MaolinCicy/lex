#ifndef __NFA2DFA_H__
#define __NFA2DFA_H__

#define for_each(n,l) for(n = l;n!=NULL;n= n->next)
#define get_tail(l,t) for(t = l;t->next!=NULL;t= t->next)

uint32_t get_state_list_size(nfa_state_value_t *state_list);

nfa_state_value_t *e_closure_state(nfa_ctx_t *ctx, nfa_state_value_t *state_value);

int nfa_event_dump(nfa_event_t *event_list);

int nfa_state_dump(nfa_state_value_t *state_list);

nfa_state_value_t *move(
    nfa_ctx_t *ctx, 
    nfa_state_value_t *state_value,
    uint32_t event_value);
#endif