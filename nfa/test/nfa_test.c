#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mempool.h"
#include "dfa.h"
#include "nfa.h"
#include "nfa2dfa.h"


#ifndef UNUSED
#define UNUSED(x)       (void)x
#endif

/*
    输入：
        event：地址
        event_value：事件值
        state_arr：跳转状态数组
        state_num：跳转状态个数
*/
static int nfa_add_event(
    nfa_ctx_t *ctx, 
    nfa_event_t * event, 
    uint32_t event_value, 
    uint32_t *state_arr, 
    uint32_t state_num)
{
    nfa_state_value_t *state_value_tmp = NULL;

    if(event == NULL)
    {
        printf("error!event is NULL!");
        return -1;
    }

    memset(event, 0x00, sizeof(nfa_event_t));

    if(state_arr == NULL)
    {
        printf("error!state_arr is NULL!");
        return -2;
    }

    if(state_num == 0)
    {
        printf("error!state_num is 0!");
        return -3;
    }

    nfa_event_t *cur_event = event;
    while(cur_event->event != 0)
    {
        if(cur_event->next == NULL)
        {
            cur_event->next = mempool_get_node(ctx->nfa_event_mp);
        }
    }
    event->event = event_value;

    for(uint32_t index = 0; index < state_num; index++)
    {
        /* 跳转到7 */
        state_value_tmp = event->jump_state;
        event->jump_state = mempool_get_node(ctx->nfa_state_value_mp);
        event->jump_state->state_value = state_arr[index];
        event->jump_state->next = state_value_tmp;
    }
    return 0;
}

/* 构造一个nfa用作测试 */

/**
状态0：起始状态
    接收 ε 切换 1、7

状态1：中间状态
    接收 ε 切换 2、4

状态2：中间状态
    接收 a 切换 3

状态3：中间状态
    接收 ε 切换 6

状态4：中间状态
    接收 b 切换 5

状态5：中间状态
    接收 ε 切换 6

状态6：中间状态
    接收 ε 切换 1、7

状态7：中间状态
    接收 a 切换 8

状态8：中间状态
    接收 b 切换 9

状态9：中间状态
    接收 b 切换 10

状态10：终止状态
    不接受字符
*/
/* 约定256是 ε */
void nfa_init(nfa_ctx_t *ctx)
{
    ctx->nfa_state_num = 11;
    ctx->nfa_state = (nfa_state_t *)malloc(ctx->nfa_state_num * sizeof(nfa_state_t));


    ctx->nfa_event_mp = mempool_create(sizeof(nfa_event_t), 257*ctx->nfa_state_num);

    ctx->nfa_state_value_mp = mempool_create(sizeof(nfa_state_value_t), 257 * ctx->nfa_state_num);

    nfa_event_t * event_tmp = NULL;
    uint32_t *state_value_arr_tmp = malloc(sizeof(uint32_t) * ctx->nfa_state_num);
    /*
    状态0：起始状态
    接收 ε 切换 1、7
    */
    ctx->nfa_state[0].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 1;
    state_value_arr_tmp[1] = 7;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 2);


    /*
    状态1：中间状态
        接收 ε 切换 2、4
    */
    ctx->nfa_state[1].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 2;
    state_value_arr_tmp[1] = 4;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 2);


    /*
    状态2：中间状态
        接收 a 切换 3
    */
    ctx->nfa_state[2].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 3;
    nfa_add_event(ctx, event_tmp, 'a', state_value_arr_tmp, 1);

    /* 构造一个环，来测试程序的处理情况 
    state_value_arr_tmp[0] = 0;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 1);*/

    /*
    状态3：中间状态
        接收 ε 切换 6
    */
    ctx->nfa_state[3].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 6;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 1);

    /*
    状态4：中间状态
        接收 b 切换 5
    */
    ctx->nfa_state[4].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 5;
    nfa_add_event(ctx, event_tmp, 'b', state_value_arr_tmp, 1);

    /*
    状态5：中间状态
        接收 ε 切换 6
    */
    ctx->nfa_state[5].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 6;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 1);

    /*
    状态6：中间状态
        接收 ε 切换 1、7
    */
    ctx->nfa_state[6].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 1;
    state_value_arr_tmp[1] = 7;
    nfa_add_event(ctx, event_tmp, EMPTY_STR, state_value_arr_tmp, 2);

    /*
    状态7：中间状态
        接收 a 切换 8
    */
    ctx->nfa_state[7].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 8;
    nfa_add_event(ctx, event_tmp, 'a', state_value_arr_tmp, 1);

    /*
    状态8：中间状态
        接收 b 切换 9
    */
    ctx->nfa_state[8].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 9;
    nfa_add_event(ctx, event_tmp, 'b', state_value_arr_tmp, 1);

    /*
    状态9：中间状态
        接收 b 切换 10
    */
    ctx->nfa_state[9].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);

    state_value_arr_tmp[0] = 10;
    nfa_add_event(ctx, event_tmp, 'b', state_value_arr_tmp, 1);

    /*
    状态10：终止状态
        不接受字符
    */
    ctx->nfa_state[10].event_list = event_tmp = mempool_get_node(ctx->nfa_event_mp);
}


void nfa_cfx_dump(nfa_ctx_t *ctx)
{
    for(uint32_t index = 0; index < ctx->nfa_state_num; index++)
    {
        printf("index:%4d,", index);

        nfa_event_t *event_tmp = ctx->nfa_state[index].event_list;
        nfa_state_value_t *nfa_state_tmp = NULL;
        while(event_tmp != NULL)
        {
            
            printf("event:%8d,", event_tmp->event);
            nfa_state_tmp = event_tmp->jump_state;
            printf("nfa_state:");
            while(nfa_state_tmp != NULL)
            {
                
                printf("%8d,", nfa_state_tmp->state_value);
                nfa_state_tmp = nfa_state_tmp->next;
            }
            event_tmp = event_tmp->next;
        }
        printf("\r\n");
    }

}

/*  
    判断两个状态链表中的状态是否完全相同
    return:
        0：不同
        1：相同
*/
uint32_t state_list_equal(
    nfa_state_value_t * list_a,
    nfa_state_value_t * list_b)
{
    nfa_state_value_t *list_a_node = NULL;
    nfa_state_value_t *list_b_node = NULL;

    if(get_state_list_size(list_a) != get_state_list_size(list_b))
    {
        return 0;
    }

    /* 初始化为节点不在链表中，知道找到一个相同的节点，则置为在 */
    uint32_t is_node_in_list = 0;
    for_each(list_a_node, list_a)
    {
        is_node_in_list = 0;
        for_each(list_b_node, list_b)
        {
            if(list_a_node->state_value == list_b_node->state_value)
            {
                is_node_in_list = 1;
                break;
            }
        }
        /* 找了一遍还不在 */
        if(is_node_in_list == 0)
        {
            /* 返回不同 */
            return 0;
        }
    }
    /* 所有节点都能匹配上，就返回相同 */
    return 1;
}

/*  判断状态链表在不在状态表格中
    return:
        0：不在
        1：在 
*/
uint32_t is_state_list_in_table(
    nfa_state_value_t **state_table,
    uint32_t table_size,
    nfa_state_value_t *state_list)
{
    for(uint32_t table_index = 0;table_index<table_size;table_index++)
    {
        if(state_list_equal(state_table[table_index], state_list))
        {
            /* table中有 一个链表与输入的链表相同，就返回链表在表格中 */
            return 1;
        }
    }
    /* 找遍了还没找到，就返回不在 */
    return 0;
}

void *list_sort(nfa_state_value_t * list)
{
    nfa_state_value_t * node = NULL;
    nfa_state_value_t * node_1 = NULL;
    uint32_t state_value_tmp = 0;
    for_each(node, list)
    {
        /* 这里需要修改 TODO */
        for_each(node_1, list)
        {
            if(node_1->next != NULL)
            {
                if(node_1->state_value > node_1->next->state_value)
                {
                    state_value_tmp = node_1->state_value;
                    node_1->state_value = node_1->next->state_value;
                    node_1->next->state_value = state_value_tmp;
                }
            }
        }
    }
    return 0;
}
int main(int argc, char ** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    printf("file:%s, build time:%s,%s\r\n", __FILE__, __DATE__, __TIME__);
    nfa_ctx_t me;
    nfa_init(&me);
    nfa_cfx_dump(&me);

    nfa_state_value_t * move_state_table[11*11];

    uint32_t move_state_table_index_in = 0;
    uint32_t move_state_table_index_out = 0;

    memset(move_state_table, 0x00, sizeof(move_state_table));

    /* 计算起始状态集合 */
    nfa_state_value_t *start_state = mempool_get_node(me.nfa_state_value_mp);
    if(start_state == NULL)
    {
        return -1;
    }
    start_state->next = NULL;
    /* 设置起始状态 */
    start_state->state_value = 0;

    move_state_table[move_state_table_index_in] = e_closure_state(&me, start_state);
    move_state_table_index_in++;

    nfa_state_value_t *state_value_tmp = NULL;
    uint32_t event_value[2] = {'a', 'b'};

    while(move_state_table_index_out < move_state_table_index_in)
    {
        /* 先计算出一个状态链表 */
        for(uint32_t i=0;i<sizeof(event_value)/sizeof(uint32_t);i++)
        {
            state_value_tmp = move(&me, move_state_table[move_state_table_index_out], event_value[i]);
            if(!is_state_list_in_table(move_state_table, move_state_table_index_in, state_value_tmp))
            {
                move_state_table[move_state_table_index_in] = state_value_tmp;
                move_state_table_index_in++;
            }
        }
        move_state_table_index_out++;
    }

    for(uint32_t i=0;i<move_state_table_index_in;i++)
    {
        printf("index:%d\r\n", i);
        list_sort(move_state_table[i]);
        nfa_state_dump(move_state_table[i]);
        printf("\r\n\r\n");
    }
    return 0;
}