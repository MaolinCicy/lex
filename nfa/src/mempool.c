#include <stdlib.h>
#include "mempool.h"

/*
node_size:一个元素的数据长度
pool_size:元素的个数
*/
mempool_t *mempool_create(uint32_t block_size, uint32_t pool_size)
{
    /* 申请内存 */
    uint64_t malloc_size = sizeof(mempool_t) + (block_size + sizeof(mempool_node_t))*pool_size;
    mempool_t *mp = (mempool_t *)malloc(malloc_size);

    /* 设置每一块的大小 */
    mp->block_size = block_size;

    /* 设置块的个数 */
    mp->pool_size = pool_size;

    /* 计算数据基址 */
    mp->base_addr = (void *)(uint64_t)mp + sizeof(mempool_t);

    /* 初始化链表结构 */
    mempool_node_t *cur_node = mp->free_node_list = (mempool_node_t *)((uint64_t)mp + sizeof(mempool_t));
    cur_node->data_addr = (void *)((uint64_t)mp + sizeof(mempool_t)) + sizeof(mempool_node_t);
    cur_node->next = NULL;

    for(uint32_t index=1; index < pool_size; index++)
    {
        cur_node->next = (mempool_node_t *)((uint64_t)mp + sizeof(mempool_t) + (sizeof(mempool_node_t) + block_size) * index);
        cur_node = cur_node->next;
        cur_node->data_addr = (void *)((uint64_t)cur_node + sizeof(mempool_node_t));
    }

    return mp;
}


void *mempool_get_node(mempool_t * mp)
{
    if(mp->free_node_list == NULL)
    {
        return NULL;
    }

    void *cur_node = mp->free_node_list;
    mp->free_node_list = mp->free_node_list->next;

    return (void *)cur_node + sizeof(mempool_node_t);
}

int mempool_put_node(mempool_t * mp, void * mp_node)
{
    mempool_node_t *node = mp_node - sizeof(mempool_node_t);

    node->next = mp->free_node_list;
    mp->free_node_list = node;
    return 0;
}

int *mempool_destory(mempool_t * mp)
{
    free(mp);
    return 0;
}


