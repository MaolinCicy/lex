#include <stddef.h>
#include <stdio.h>
#include "mempool.h"
#include "dfa.h"
#include "nfa.h"
#include "nfa2dfa.h"


uint32_t get_state_list_size(nfa_state_value_t *state_list)
{
    uint32_t ret = 0;
    for(;state_list!=NULL;state_list=state_list->next)
        ret++;
    return ret;
}

/* return:
    0：状态不在状态集合中
    1：状态在状态集合中 */
uint8_t is_state_in(uint32_t state_value, nfa_state_value_t *state_list)
{
    nfa_state_value_t *state = NULL;
    uint8_t ret_flag = 0;
    for_each(state, state_list)
    {
        if(state->state_value == state_value)
        {
            ret_flag = 1;
            break;
        }
    }
    return ret_flag;
}

/* 将一个状态值添加到状态集合中（重复则不添加） */
uint8_t add_state_to_list(
    nfa_ctx_t *ctx, 
    uint32_t state_value, 
    nfa_state_value_t *state_list)
{
    if(!is_state_in(state_value, state_list))
    {
        nfa_state_value_t *tail = NULL;
        if(state_list == NULL)
        {
            state_list = tail = mempool_get_node(ctx->nfa_state_value_mp);
        }
        else
        {
            get_tail(state_list, tail);
            tail->next = mempool_get_node(ctx->nfa_state_value_mp);
            tail = tail->next;
        }
        tail->next = NULL;
        tail->state_value = state_value;
    }
    return 0;
}

/* 
    输入：一个NFA
          NFA中的一个状态
    输出：能够从输入状态只通过空串转换到达的状态集合 */
nfa_state_value_t *e_closure_state(
    nfa_ctx_t *ctx, 
    nfa_state_value_t *state_value)
{
    /* 返回的状态链表 */
    nfa_state_value_t *ret_state = NULL;
    nfa_state_value_t *state_tmp = NULL;
    nfa_state_value_t *state_tail = NULL;
    nfa_state_value_t *n = NULL;

    /* 先将输入状态添加到返回链表中 */
    for_each(n, state_value)
    {
        state_tmp = mempool_get_node(ctx->nfa_state_value_mp);
        state_tmp->state_value = n->state_value;
        state_tmp->next = ret_state;
        ret_state = state_tmp;
    }

    state_tail = ret_state;
    while(state_tail->next != NULL)
    {
        state_tail = state_tail->next;
    }

    /* 对输入的每个需要处理的状态 */
    for_each(n, ret_state)
    {
        nfa_state_t *nfa_state = &(ctx->nfa_state[n->state_value]);

        nfa_event_t *event = NULL;
        
        /* 对于当前状态能接受的每个事件 */
        for_each(event, nfa_state->event_list)
        {
            /* 事件为空串，是我们需要的事件 */
            if(event->event == EMPTY_STR)
            {
                /* 将该事件能跳转的所有状态添加到返回链表中 */
                for_each(state_tmp, event->jump_state)
                {
                    add_state_to_list(ctx, state_tmp->state_value, ret_state);
                }
            }
        }
    }
    return ret_state;
}

nfa_state_value_t *move(
    nfa_ctx_t *ctx, 
    nfa_state_value_t *state_value,
    uint32_t event_value)
{
    /* 返回的状态链表 */
    nfa_state_value_t *ret_state = NULL;
    nfa_state_value_t *state_tmp = NULL;
    nfa_state_value_t *n = NULL;

    /* 对于输入状态中的每一个状态 */
    for_each(n, state_value)
    {
        nfa_state_t *nfa_state = &(ctx->nfa_state[n->state_value]);

        nfa_event_t *event = NULL;
        
        /* 对于当前状态能接受的每个事件 */
        for_each(event, nfa_state->event_list)
        {
            /* 匹配到事件 */
            if(event->event == event_value)
            {
                /* 将该事件能跳转的所有状态添加到返回链表中 */
                for_each(state_tmp, event->jump_state)
                {
                    if(ret_state == NULL)
                    {
                        ret_state = mempool_get_node(ctx->nfa_state_value_mp);
                        ret_state->state_value = state_tmp->state_value;
                        ret_state->next = NULL;
                    }
                    else
                    {
                        add_state_to_list(ctx, state_tmp->state_value, ret_state);
                    }

                }
            }
        }
    }
    if(ret_state != NULL)
    {
        return e_closure_state(ctx, ret_state);
    }
    else
    {
        return NULL;
    }
}
int nfa_event_free(nfa_ctx_t *ctx, nfa_event_t *event_list)
{
    nfa_event_t *event_tmp1 = event_list;
    nfa_event_t *event_tmp2 = event_list;
    nfa_state_value_t *state_tmp1 = NULL;
    nfa_state_value_t *state_tmp2 = NULL;
    while(event_tmp1 != NULL)
    {
        event_tmp1 = event_tmp1->next;
        

        state_tmp1 = state_tmp2 = event_tmp1->jump_state;
        while(state_tmp1 != NULL)
        {
            state_tmp1 = state_tmp1->next;
            mempool_put_node(ctx->nfa_event_mp, state_tmp2);
            state_tmp2 = state_tmp1;
        }
        mempool_put_node(ctx->nfa_event_mp, event_tmp2);
        event_tmp2 = event_tmp1;
    }
    return 0;
}

int nfa_event_dump(nfa_event_t *event_list)
{
    nfa_event_t *event_tmp = event_list;
    nfa_state_value_t *state_tmp = NULL;

    while(event_tmp != NULL)
    {
        printf("event:%d",event_tmp->event);

        state_tmp = event_tmp->jump_state;
        while(state_tmp != NULL)
        {
            printf("event:%d",event_tmp->event);
            state_tmp = state_tmp->next;
        }
        event_tmp = event_tmp->next;
        printf("\r\n");
    }

    return 0;
}

int nfa_state_dump(nfa_state_value_t *state_list)
{
    nfa_state_value_t *state_value = state_list;
    printf("state_value:");
    while(state_value != NULL)
    {
        printf("%4d,", state_value->state_value);
        state_value = state_value->next;
    }
    printf("\r\n");
    return 0;
}
/* 
    输入：一个NFA
          NFA中的一个状态集合
    输出：能够从输入状态集合只通过空串转换到达的状态集合 */
void e_closure_state_collection(void)
{

}


